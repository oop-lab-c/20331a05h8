abstract class  Animal{
    abstract void cat();
    void dog(){
        System.out.println("i am dog\t");
    }
}
class derived extends Animal{
    void cat(){
         System.out.println("i am cat");
    }
    
}
class Main{
    public static void main(String[] args){
        derived obj=new derived();
        obj.cat();
        obj.dog();
    }
}
