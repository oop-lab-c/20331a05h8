class MethodORJava {
    public void display(int a) {
        System.out.println("the number is "+a);
    }
}
class Derived extends MethodORJava {
    public void display(int a) {
          System.out.println("the number is "+a);
    }
    public static void main(String args[]) {
        Derived obj = new Derived();
        obj.display(7);
    }
}
