interface bbl {
    void display();
}
class PureAbsJava implements bbl {
    public void display() {
        System.out.println("I am from bbl");
    }
    public static void main(String[] args) {
        PureAbsJava obj = new PureAbsJava();
       obj.display();
    }
}
