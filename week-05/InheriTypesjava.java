class bird
{
    void fly()
    {
        System.out.println("I fly with my wings");
    }
}
class parrot extends bird
{
    void eat()
    {
        System.out.println("I eat my food");
    }
}
class childparrot extends parrot // multiple inheitance 
{
    public static void main(String[] args)
    {
        childparrot p = new childparrot();
        p.fly();
        p.eat();
    }
}
class crow extends bird // simple inheritance
{
    void colour()
    {
        System.out.println("my colour is black");
    }
    public static void main(String[] args)
    {
        crow c = new crow();
        c.fly();
        c.colour();
    }
}
