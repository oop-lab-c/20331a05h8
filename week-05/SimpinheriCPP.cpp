#include<iostream>
using namespace std;
class bird
{
    public:
    void fly()
    {
        cout<<"i fly with my wings"<<endl;
    }
};
class parrot:public bird
{
    public:
    void eat()
    {
        cout<<"i eat my food"<<endl;
    }
};
int main()
{
    parrot obj;
    obj.fly();
    obj.eat();
    return 0;
}